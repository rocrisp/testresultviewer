<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certified[]|\Cake\Collection\CollectionInterface $certified
 */
?>

<div class="certified index large-9 medium-8 columns content">
    <h3><?= __('Certified Operator CVP Results') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('version') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('log') ?></th>
                <th scope="col"><?= $this->Paginator->sort('build_date') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($certified as $certified): ?>
            <tr>
                <td><?= $this->Number->format($certified->id) ?></td>
                <td><?= h($certified->name) ?></td>
                <td><?= h($certified->type) ?></td>
                <td><?= h($certified->version) ?></td>
                <td><?= h($certified->status) ?></td>
                <td><?= $this->Html->link(__('View log'), h($certified->log)) ?></td>
                <td><?= h($certified->build_date) ?></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
