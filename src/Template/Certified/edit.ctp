<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certified $certified
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $certified->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $certified->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Certified'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="certified form large-9 medium-8 columns content">
    <?= $this->Form->create($certified) ?>
    <fieldset>
        <legend><?= __('Edit Certified') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type');
            echo $this->Form->control('version');
            echo $this->Form->control('status');
            echo $this->Form->control('log');
            echo $this->Form->control('build_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
