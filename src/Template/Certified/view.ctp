<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Certified $certified
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Certified'), ['action' => 'edit', $certified->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Certified'), ['action' => 'delete', $certified->id], ['confirm' => __('Are you sure you want to delete # {0}?', $certified->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Certified'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Certified'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="certified view large-9 medium-8 columns content">
    <h3><?= h($certified->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($certified->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($certified->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Version') ?></th>
            <td><?= h($certified->version) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($certified->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Log') ?></th>
            <td><?= h($certified->log) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($certified->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Build Date') ?></th>
            <td><?= h($certified->build_date) ?></td>
        </tr>
    </table>
</div>
