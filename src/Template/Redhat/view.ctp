<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Redhat $redhat
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Redhat'), ['action' => 'edit', $redhat->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Redhat'), ['action' => 'delete', $redhat->id], ['confirm' => __('Are you sure you want to delete # {0}?', $redhat->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Redhat'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Redhat'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="redhat view large-9 medium-8 columns content">
    <h3><?= h($redhat->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($redhat->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($redhat->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Version') ?></th>
            <td><?= h($redhat->version) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($redhat->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Log') ?></th>
            <td><?= h($redhat->log) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($redhat->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Build Date') ?></th>
            <td><?= h($redhat->build_date) ?></td>
        </tr>
    </table>
</div>
