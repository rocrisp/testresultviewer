<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Redhat[]|\Cake\Collection\CollectionInterface $redhat
 */
?>

<div class="redhat index large-9 medium-8 columns content">
    <h3><?= __('Red Hat Operator CVP Results') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('version') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('log') ?></th>
                <th scope="col"><?= $this->Paginator->sort('build_date') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($redhat as $redhat): ?>
            <tr>
                <td><?= $this->Number->format($redhat->id) ?></td>
                <td><?= h($redhat->name) ?></td>
                <td><?= h($redhat->type) ?></td>
                <td><?= h($redhat->version) ?></td>
                <td><?= h($redhat->status) ?></td>
                <td><?= $this->Html->link(__('View log'), h($redhat->log)) ?></td>
                <td><?= h($redhat->build_date) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
