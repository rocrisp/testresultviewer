<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Redhat $redhat
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $redhat->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $redhat->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Redhat'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="redhat form large-9 medium-8 columns content">
    <?= $this->Form->create($redhat) ?>
    <fieldset>
        <legend><?= __('Edit Redhat') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type');
            echo $this->Form->control('version');
            echo $this->Form->control('status');
            echo $this->Form->control('log');
            echo $this->Form->control('build_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
