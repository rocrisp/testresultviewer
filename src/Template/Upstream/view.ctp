<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upstream $upstream
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Upstream'), ['action' => 'edit', $upstream->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Upstream'), ['action' => 'delete', $upstream->id], ['confirm' => __('Are you sure you want to delete # {0}?', $upstream->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Upstream'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Upstream'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="upstream view large-9 medium-8 columns content">
    <h3><?= h($upstream->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($upstream->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= h($upstream->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Version') ?></th>
            <td><?= h($upstream->version) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($upstream->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Log') ?></th>
            <td><?= h($upstream->log) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($upstream->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Build Date') ?></th>
            <td><?= h($upstream->build_date) ?></td>
        </tr>
    </table>
</div>
