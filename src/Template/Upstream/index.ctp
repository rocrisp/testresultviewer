<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upstream[]|\Cake\Collection\CollectionInterface $upstream
 */
?>

<div class="upstream index large-9 medium-8 columns content">
    <h3><?= __('Upstream Operator CVP Results') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('version') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('log') ?></th>
                <th scope="col"><?= $this->Paginator->sort('build_date') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($upstream as $upstream): ?>
            <tr>
                <td><?= $this->Number->format($upstream->id) ?></td>
                <td><?= h($upstream->name) ?></td>
                <td><?= h($upstream->type) ?></td>
                <td><?= h($upstream->version) ?></td>
                <td><?= h($upstream->status) ?></td>
                <td><?= $this->Html->link(__('View log'), h($upstream->log)) ?></td>
                <td><?= h($upstream->build_date) ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
