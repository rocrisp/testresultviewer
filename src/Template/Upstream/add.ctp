<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Upstream $upstream
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Upstream'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="upstream form large-9 medium-8 columns content">
    <?= $this->Form->create($upstream) ?>
    <fieldset>
        <legend><?= __('Add Upstream') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('type');
            echo $this->Form->control('version');
            echo $this->Form->control('status');
            echo $this->Form->control('log');
            echo $this->Form->control('build_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
