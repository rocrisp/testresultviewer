<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Community[]|\Cake\Collection\CollectionInterface $community
 */
?>

<div class="community index large-9 medium-8 columns content">
    <h3><?= __('Community Operator CVP Results') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('version') ?></th>
                <th scope="col"><?= $this->Paginator->sort('status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('log') ?></th>
                <th scope="col"><?= $this->Paginator->sort('build_date') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($community as $community): ?>
            <tr>
                <td><?= $this->Number->format($community->id) ?></td>
                <td><?= h($community->name) ?></td>
                <td><?= h($community->type) ?></td>
                <td><?= h($community->version) ?></td>
                <td><?= h($community->status) ?></td>
                <td><?= $this->Html->link(__('View log'), h($community->log)) ?></td>
                <td><?= h($community->build_date) ?></td>
                
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
