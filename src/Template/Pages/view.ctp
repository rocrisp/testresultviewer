<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('home.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>

<body class="home">
<div class="text-center">    
<h1>Operator Test Result Summary Page</h1>
</div>
<div class="row">
<table>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Version</th>
        <th>Status</th>
        <th>BuildDate</th>
        <th>Log</th>
    </tr>


<?php
   foreach($viewData as $row)
   {?>
      <tr>
          <td>
             <?php echo $row->name;?>
          </td>
          <td>
          <?php echo $row->type;?>
          </td>   
          <td>
          <?php echo $row->version;?>
          </td>
          <td>
          <?php echo $row->status;?>
          </td>
          <td>
          <?php echo $row->build_date;?>
          </td>
          <td>
          <?php echo "<a href=\"".$row->log."\"> log </a>";?>
          </td>
   </tr>
       
   <?php }?>
</table>
   </div>
</body>
</html>