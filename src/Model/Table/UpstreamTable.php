<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Upstream Model
 *
 * @method \App\Model\Entity\Upstream get($primaryKey, $options = [])
 * @method \App\Model\Entity\Upstream newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Upstream[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Upstream|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upstream saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Upstream patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Upstream[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Upstream findOrCreate($search, callable $callback = null, $options = [])
 */
class UpstreamTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('upstream');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 150)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->scalar('type')
            ->maxLength('type', 150)
            ->requirePresence('type', 'create')
            ->notEmptyString('type');

        $validator
            ->scalar('version')
            ->maxLength('version', 150)
            ->requirePresence('version', 'create')
            ->notEmptyString('version');

        $validator
            ->scalar('status')
            ->maxLength('status', 150)
            ->requirePresence('status', 'create')
            ->notEmptyString('status');

        $validator
            ->scalar('log')
            ->maxLength('log', 150)
            ->requirePresence('log', 'create')
            ->notEmptyString('log');

        $validator
            ->date('build_date')
            ->requirePresence('build_date', 'create')
            ->notEmptyDate('build_date');

        return $validator;
    }
}
