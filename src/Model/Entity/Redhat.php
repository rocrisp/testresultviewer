<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Redhat Entity
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $version
 * @property string $status
 * @property string $log
 * @property \Cake\I18n\FrozenDate $build_date
 */
class Redhat extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'type' => true,
        'version' => true,
        'status' => true,
        'log' => true,
        'build_date' => true,
    ];
}
