<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Certified Controller
 *
 * @property \App\Model\Table\CertifiedTable $Certified
 *
 * @method \App\Model\Entity\Certified[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CertifiedController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $certified = $this->paginate($this->Certified);

        $this->set(compact('certified'));
    }

    /**
     * View method
     *
     * @param string|null $id Certified id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $certified = $this->Certified->get($id, [
            'contain' => [],
        ]);

        $this->set('certified', $certified);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $certified = $this->Certified->newEntity();
        if ($this->request->is('post')) {
            $certified = $this->Certified->patchEntity($certified, $this->request->getData());
            if ($this->Certified->save($certified)) {
                $this->Flash->success(__('The certified has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The certified could not be saved. Please, try again.'));
        }
        $this->set(compact('certified'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Certified id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $certified = $this->Certified->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $certified = $this->Certified->patchEntity($certified, $this->request->getData());
            if ($this->Certified->save($certified)) {
                $this->Flash->success(__('The certified has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The certified could not be saved. Please, try again.'));
        }
        $this->set(compact('certified'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Certified id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $certified = $this->Certified->get($id);
        if ($this->Certified->delete($certified)) {
            $this->Flash->success(__('The certified has been deleted.'));
        } else {
            $this->Flash->error(__('The certified could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
