<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Redhat Controller
 *
 * @property \App\Model\Table\RedhatTable $Redhat
 *
 * @method \App\Model\Entity\Redhat[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RedhatController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $redhat = $this->paginate($this->Redhat);

        $this->set(compact('redhat'));
    }

    /**
     * View method
     *
     * @param string|null $id Redhat id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $redhat = $this->Redhat->get($id, [
            'contain' => [],
        ]);

        $this->set('redhat', $redhat);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $redhat = $this->Redhat->newEntity();
        if ($this->request->is('post')) {
            $redhat = $this->Redhat->patchEntity($redhat, $this->request->getData());
            if ($this->Redhat->save($redhat)) {
                $this->Flash->success(__('The redhat has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The redhat could not be saved. Please, try again.'));
        }
        $this->set(compact('redhat'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Redhat id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $redhat = $this->Redhat->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $redhat = $this->Redhat->patchEntity($redhat, $this->request->getData());
            if ($this->Redhat->save($redhat)) {
                $this->Flash->success(__('The redhat has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The redhat could not be saved. Please, try again.'));
        }
        $this->set(compact('redhat'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Redhat id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $redhat = $this->Redhat->get($id);
        if ($this->Redhat->delete($redhat)) {
            $this->Flash->success(__('The redhat has been deleted.'));
        } else {
            $this->Flash->error(__('The redhat could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
