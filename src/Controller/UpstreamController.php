<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Upstream Controller
 *
 * @property \App\Model\Table\UpstreamTable $Upstream
 *
 * @method \App\Model\Entity\Upstream[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UpstreamController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $upstream = $this->paginate($this->Upstream);

        $this->set(compact('upstream'));
    }

    /**
     * View method
     *
     * @param string|null $id Upstream id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $upstream = $this->Upstream->get($id, [
            'contain' => [],
        ]);

        $this->set('upstream', $upstream);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $upstream = $this->Upstream->newEntity();
        if ($this->request->is('post')) {
            $upstream = $this->Upstream->patchEntity($upstream, $this->request->getData());
            if ($this->Upstream->save($upstream)) {
                $this->Flash->success(__('The upstream has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The upstream could not be saved. Please, try again.'));
        }
        $this->set(compact('upstream'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Upstream id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $upstream = $this->Upstream->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $upstream = $this->Upstream->patchEntity($upstream, $this->request->getData());
            if ($this->Upstream->save($upstream)) {
                $this->Flash->success(__('The upstream has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The upstream could not be saved. Please, try again.'));
        }
        $this->set(compact('upstream'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Upstream id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $upstream = $this->Upstream->get($id);
        if ($this->Upstream->delete($upstream)) {
            $this->Flash->success(__('The upstream has been deleted.'));
        } else {
            $this->Flash->error(__('The upstream could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
