mpod=mysql-1-8xzpr

echo "Copying setup files into pod..."
oc cp ./create_table.sql $mpod:/tmp/create_table.sql

echo "Creating table(s)..."
oc exec $mpod -- bash -c "mysql --user=root default < /tmp/create_table.sql"

echo "Describe table(s)..."

echo "Describe redhat"
oc exec $mpod -- bash -c "mysql --user=root default -e 'use default; describe redhat;'"

echo "Describe certified"
oc exec $mpod -- bash -c "mysql --user=root default -e 'use default; describe certified;'"

echo "Describe upstream"
oc exec $mpod -- bash -c "mysql --user=root default -e 'use default; describe upstream;'"

echo "Describe community"
oc exec $mpod -- bash -c "mysql --user=root default -e 'use default; describe community;'"

# Temporary fix because MySQL 8.* client isn't secure in mysqljs Nodejs module
#echo "Setting user password..."
#oc exec $mpod -- bash -c "mysql --user=root -e 'ALTER USER '\''userBMN'\'' IDENTIFIED WITH mysql_native_password BY '\''vRUWhkydWOKdMDLd'\'';'"

# Create user and password for jenkins
oc exec $mpod -- bash -c "mysql --user=root default -e 'CREATE USER '\''jenkins'\'' IDENTIFIED BY '\''jenkins'\'';'"

# Grant user priviledges
oc exec $mpod -- bash -c "mysql --user=root default -e 'GRANT ALL PRIVILEGES ON *.* TO '\''jenkins'\'' WITH GRANT OPTION;'"

#echo "Flushing privileges..."
oc exec $mpod -- bash -c "mysql --user=root -e 'FLUSH PRIVILEGES;'"