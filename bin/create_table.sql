USE default;

CREATE TABLE IF NOT EXISTS redhat 
(
    id  INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
    name  VARCHAR( 150 ) NOT NULL,
    type  VARCHAR( 150 ) NOT NULL,
    version VARCHAR( 150 ) NOT NULL,
    status VARCHAR( 150 ) NOT NULL,
    log VARCHAR( 300 ) NOT NULL,
    build_date DATE NOT NULL 
    );

CREATE TABLE IF NOT EXISTS certified 
(
    id   INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
    name  VARCHAR( 150 ) NOT NULL,
    type  VARCHAR( 150 ) NOT NULL,
    version VARCHAR( 150 ) NOT NULL,
    status VARCHAR( 150 ) NOT NULL,
    log VARCHAR( 300 ) NOT NULL,
    build_date DATE NOT NULL 
    );

CREATE TABLE IF NOT EXISTS community 
(
    id    INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
    name  VARCHAR( 150 ) NOT NULL,
    type  VARCHAR( 150 ) NOT NULL,
    version VARCHAR( 150 ) NOT NULL,
    status VARCHAR( 150 ) NOT NULL,
    log VARCHAR( 300 ) NOT NULL,
    build_date DATE NOT NULL 
    );

CREATE TABLE IF NOT EXISTS upstream 
(
    id    INT AUTO_INCREMENT NOT NULL PRIMARY KEY, 
    name  VARCHAR( 150 ) NOT NULL,
    type  VARCHAR( 150 ) NOT NULL,
    version VARCHAR( 150 ) NOT NULL,
    status VARCHAR( 150 ) NOT NULL,
    log VARCHAR( 300 ) NOT NULL,
    build_date DATE NOT NULL 
    );

